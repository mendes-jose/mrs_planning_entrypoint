This README does not contain any confidential information.
It can be publicly accessed at <https://gitlab.com/mendes-jose/mrs_planning_entrypoint>.

> We assume your are using a Linux OS (Ubuntu/Lubuntu 16.04 64bit is preferable).
> It can be running natively or on a VM.
> If running in a VM make sure to install the Virtual Box Guest Additions for Linux.
> Keep in mind that running Gazebo simulation (which is part of the project) on a VM may not work very well.

# mrs_planning_entrypoint

1.  Get access to the private project:

    Either ask project manager to add you to the project, or get valid keys:

    
    *   By simply coping:
        ```sh
        $ mkdir ~/.ssh
        $ cp <key_path>/id_rsa <key_path>/id_rsa.pub ~/.ssh
        $ cd ~/.ssh
        $ sudo chmod 644 id_rsa.pub
        $ sudo chmod 600 id_rsa
        ```
    
    *   By ssh coping from another machine:
    
        ```sh
        $ scp -r mnds@10.8.37.149:~/.ssh  ~/
        ```

2.  Install git:

    ```sh
    $ sudo apt-get update
    $ sudo apt-get upgrade
    $ sudo apt-get install -y git
    ```

3.  Clone the project:

    ```sh
    $ mkdir -p ~/ros_ws/src
    $ cd ~/ros_ws/src
    $ git clone git@gitlab.com:cea_lri_nav/mrs_planning.git
    ```

4.  Open project documentation:

    ```sh
    $ firefox ~/ros_ws/src/mrs_planning/README.md.html &>/dev/null &
    ```